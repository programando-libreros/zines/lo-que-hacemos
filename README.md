---
title: 'Cómo lo hacemos: las herramientas que nos hicieron'
tags: ruweb, programación literaria, pecas, pub, export-pdf, prolibreros
---

:::warning
- [ ] Documentar la reproducción desde GitLab.
- [ ] Cambiar el usuario git de la reproducción automatizada.
- [ ] ¿Dejar en «Varios autores» en lugar de cada nombre?
- [ ] Reescribir descripción.
- [ ] Completar y editar esta receta.
- [ ] Explicar qué es Pecas legacy (incluyendo historia).
- [ ] Explicar qué es ExportPDF (incluyendo historia)
- [ ] Probablemente se tengan que modificar las plantillas CSS y TeX.
- [ ] Añadir en ExportPDF configuración de tamaño de página para folleto.
:::

# Cómo lo hacemos: las herramientas que nos hicieron

Esta receta reproduce el EPUB, PDF, PDF (folleto) y MOBI de _Lo que hacemos:_ software _libre y acceso abierto_. Para su reproducción se usa [Pecas](https://programando.li/bros/)[^1] y [ExportPDF](https://gitlab.com/-/snippets/1917490), herramientas editoriales usadas por Programando LIBREros para la publicación automatizada, multiformato y con _software_ libre o de código abierto. Este archivo es la documentación del proceso, una historia técnica de nuestra unidad de trabajo y el texto ejecutable para llevar a cabo la reproducción.

[^1]: «Pecas» en este texto hace referencia a [Pecas _legacy_](https://gitlab.com/programando-libreros/herramientas/pecas-legacy/), primera implementación de Pecas.

## Archivos de descarga

Nuestro resultado del proceso de reproducción es accesible en los siguientes enlaces:

* [EPUB](https://gitlab.com/programando-libreros/zines/lo-que-hacemos/-/raw/master/out/lo-que-hacemos.epub?inline=false)
* [PDF](https://gitlab.com/programando-libreros/zines/lo-que-hacemos/-/raw/master/out/lo-que-hacemos.pdf?inline=false)
* [PDF (folleto)](https://gitlab.com/programando-libreros/zines/lo-que-hacemos/-/raw/master/out/lo-que-hacemos_folleto.pdf?inline=false)
* [MOBI](https://gitlab.com/programando-libreros/zines/lo-que-hacemos/-/raw/master/out/lo-que-hacemos.mobi?inline=false)

## Manual de uso

Esta publicación puede reproducirse de dos maneras:

1. Desde [GitLab](https://gitlab.com/).
2. En local.

### Reproducción desde GitLab

Si no cuentas con equipo de cómputo o este no satisface los requisitos, te recomendamos esta modalidad. Al reproducir a través de GitLab solo se requiere  de [una cuenta](https://gitlab.com/users/sign_in) en esa plataforma.

==TODO==

### Reproducción local

Para la reproducción local se requiere de una computadora con los siguientes requisitos:

* [Bash](https://www.gnu.org/software/bash/)
* [ExportPDF](https://gitlab.com/-/snippets/1917490)
* [Git](https://git-scm.com/)
* [Pandoc](https://pandoc.org/)
* [Pecas](https://programando.li/bros/)
* [Ruby](https://www.ruby-lang.org)
* [RuWEB](https://rubygems.org/gems/ruweb)
* [TeX Live](https://tug.org/texlive/)

Satisfechos, basta con el siguiente comando para reproducir _Lo que hacemos_:

```shell= x
ruweb https://pad.programando.li/breros:como-lo-hacemos/download
```

## Procedimiento

Ambas modalidades (desde GitLab o en local) para reproducir _Lo que hacemos_ ejecutan este documento con [RuWEB](https://pad.programando.li/ruweb):

> un [preprocesador](https://en.wikipedia.org/wiki/Preprocessor) de [programación literaria](http://www.literateprogramming.com/knuthweb.pdf) multilingüe y [colaborativo en tiempo cuasireal](https://en.wikipedia.org/wiki/Collaborative_real-time_editor) a partir de [pads](https://en.wikipedia.org/wiki/Notebook) con sintaxis [Markdown](https://es.wikipedia.org/wiki/Markdown). Es decir, RuWEB permite ejecutar textos en Markdown en cuyo proceso de escritura puede usarse una plataforma de pads como [Etherpad](https://etherpad.org), [HackMD](https://hackmd.io), [CodiMD](https://github.com/hackmdio/codimd), [HedgeDoc](https://hedgedoc.org) o [CryptPad](https://cryptpad.fr). RuWEB es el «estado del arte» de la unidad de trabajo [Programando LIBREros](https://programando.li/breros) en su búsqueda por publicar, desde una sola fuente y con _software_ libre o de código abierto ([FOSS](https://es.wikipedia.org/wiki/Software_libre_y_de_c%C3%B3digo_abierto), por sus siglas en inglés), una multiplicidad de formatos de manera automatizada. Esta punta de lanza de dicha unidad es una respuesta a las limitaciones técnicas que se nos empezaron a ser patentes con el pasar de los años.

RuWEB es parte de la segunda generación de herramientas que estamos empezando a usar en Programando LIBREros. La primera generación fueron herramientas como Pecas y ExportPDF. Antes de RuWEB, la documentación y el código de nuestras herramientas estaban esparcidos entre archivos en un repositorio [Git](https://es.wikipedia.org/wiki/Git). En la actualidad, estas están contenidas en un solo pad que es ejecutable con RuWEB.

Por interoperatibilidad y para archivo, esta receta utiliza [el repositorio](https://gitlab.com/programando-libreros/zines/lo-que-hacemos) de _Lo que hacemos_ para tener los archivos de entrada en formato [Markdown](https://en.wikipedia.org/wiki/Markdown) (que también llamamos «archivos madre» o _inputs_) y para almacenar los resultados de su conversión (que pueden denominarse «salidas» u _outputs_).

De manera general, esta receta lleva a cabo lo siguiente:

1. Descarga [el repositorio](https://gitlab.com/programando-libreros/zines/lo-que-hacemos) de _Lo que hacemos_.
2. Prepara los archivos madre para Pecas y ExportPDF.
3. Produce los formatos EPUB y MOBI con Pecas.
4. Produce los PDF para lectura en pantalla y para impresión como folleto con ExportPDF.
5. Produce el sitio de la publicación.

Puntualmente, este es el proceso de ejecución:

```sh=
_descarga_repo
_prepara_repo
_arregla_inputs
_guarda_config_Pecas
_descarga_plantilla_css
_produce_ebooks
_descarga_plantilla_pdf
_produce_pdfs
_produce_sitio
_mueve_outputs
_limpia_repo
```

### Preconfiguración

Todos los archivos necesarios para la publicación de _Lo que hacemos_ están disponibles en su repositorio:

```shell= _repositorio
https://gitlab.com/programando-libreros/zines/lo-que-hacemos.git
```

:::info
¿Sabías que puedes clonar el repositorio para hacer tus publicaciones? Solo cambia la ruta anterior por la URL de tu [_fork_](https://www.tutorialspoint.com/gitlab/gitlab_fork_project.htm).
:::

Todos los archivos producidos estarán alojados en este directorio:

```shell= _directorio
lo-que-hacemos
```

A continuación se declaran metadatos comunes a todos los archivos de salida:

* Título:

```shell= _título
Lo que hacemos
```

* Subtítulo:

```shell= _subtítulo
software libre y acceso abierto
```

* Autores:

```shell= _autores
Varios autores
```

* Editorial:

```shell= _editorial
Programando LIBREros
```

* Descripción:

```shell= _descripción
Esta publicación fue elaborada para el Festival Latinoamericano de Instalación de Software Libre realizado en Colima Hacklab en abril del 2019. El motivo de esta obra es ofrecerte de primera mano algunos de los escritos «seminales» de los movimientos del software libre y del acceso abierto.
```

Con estos datos ahora es posible trasladarse al directorio de trabajo:

```shell= _descarga_repo
if [ ! -d "in" ]; then
  if [ ! -d "_directorio" ]; then
    git clone --depth 1 _repositorio _directorio
  fi
  cd _directorio
fi
```

Cuando se reproduce desde GitLab ya se está en la ubicación necesaria. Para comprobar esto se busca el directorio `in` que está dentro del repositorio. Si la carpeta `in` no es localizada, se busca el directorio del repositorio para después ingresar. Si este directorio tampoco es localizado, se procede a descargarlo con Git.

Una vez adentro, se prepara la estructura necesaria:

```shell= _prepara_repo
mkdir ebooks
mkdir pdfs
touch ebooks/.automata_init
```

Los directorios `ebooks` y `pdfs` serán lugares temporales para producir las publicaciones. Una vez que los archivos finales sean trasladados a su directorio final, estos directorios serán eliminados. Además, para que Pecas funcione de manera automatizada, es necesario un archivo oculto y con contenido irrelevante con el nombre `.automata_init`. Este fichero lo generamos dentro de `ebooks` para ahí producir automáticamente los libros electrónicos.

### Generación de _ebooks_

Pecas legacy...

Configuración de Pecas:

```yaml= _configuración_de_Pecas
---
# Generales
title: _título
subtitle: _subtítulo
author:
  - _autores
publisher:
  - _editorial
synopsis: "_descripción"
category: 
  - Software libre
  - Acceso abierto
  - Antología
language: es
version: 1.0.0
cover: cover.png
navigation: nav.xhtml

# Tabla de contenidos
no-toc: 
  - 002-legal
no-spine: 
custom: 

# Si se quiere EPUB fijo
px-width: 
px-height: 

# Fallbacks
fallback: 

# WCAG:
summary: Este EPUB está optimizado para personas con deficiencias visuales; cualquier observación por favor póngase en contacto.
mode:
  - textual
  - visual
mode-sufficient:
  - textual, visual
  - textual
feature:
  - structuralNavigation
  - alternativeText
  - resizeText
hazard:
  - none
control:
api: ARIA
```

Guarda configuración de Pecas en un archivo:

```shell= _guarda_config_Pecas
echo "_configuración_de_Pecas" > ebooks/meta-data.yaml
```

URL a la hoja de estilo para el diseño de los _ebooks_:

```shell _fuente_css
https://pad.programando.li/breros:como-lo-hacemos:css/download
```

Descarga de hoja de estilo:

```shell= _descarga_plantilla_css
curl -o in/css/styles.css _fuente_css
```

Producción de _ebooks_ con Pecas:

```shell= _produce_ebooks
pc-automata -d ebooks/ \
            -y ebooks/meta-data.yaml \
            -f in/md/for_ebooks.md \
            -c in/img/cover.png \
            -i in/img \
            -n in/md/notas.md \
            -s in/css/styles.css \
            --index 1 \
            --section \
            --no-pre \
            --no-analytics \
            --overwrite
```

### Generación de PDF

Suplemento de Pecas: ExportPDF...

URL a la plantilla para el diseño de los PDF:

```shell _fuente_tex
https://pad.programando.li/breros:como-lo-hacemos:tex/download
```

Descarga de plantilla:

```shell= _descarga_plantilla_pdf
curl -o pdfs/template.tex _fuente_tex
```

Producción de PDF para lectura:

```shell= _produce_pdf_lectura
export-pdf --geometry=7in,8.5in,1in,1.25in \
           --title="_título" \
           --author="_autores" \
           --press="_editorial" \
           --template="pdfs/template.tex" \
           --ragged-right \
           --tit-head \
           --leave-h1 \
           in/md/for_pdfs.md
```

Producción de PDF para impresión:

```shell= _produce_pdf_imposición
pdfbook2 -n -p legalpaper in/md/for_pdfs.pdf
```

:::warning
Aunque ExportPDF permite hacer imposición con `--imposition`, el tamaño de hoja que utiliza es carta, cuando en este caso se necesita oficio.
:::

Manda a producir PDF:

```shell= _produce_pdfs
_produce_pdf_lectura
_produce_pdf_imposición
```

### Arreglos

Directorio de pads:

```shell= _pads
https://pad.programando.li/breros:como-lo-hacemos:pre/download
https://pad.programando.li/breros:como-lo-hacemos:prologo/download
https://pad.programando.li/breros:como-lo-hacemos:manifiesto-gnu/download
https://pad.programando.li/breros:como-lo-hacemos:catedral-bazar/download
https://pad.programando.li/breros:como-lo-hacemos:iniciativa-budapest/download
https://pad.programando.li/breros:como-lo-hacemos:manifiesto-guerrilla/download
https://pad.programando.li/breros:como-lo-hacemos:solidaridad-libgen-scihub/download
https://pad.programando.li/breros:como-lo-hacemos:notas/download
```

Los arreglos se hacen con Ruby:

```ruby= _script
# Gemas necesarias
require 'ruweb'

# Listado de URLS a pads
pads = %W{_pads}

# Descarga de pads
pads.map! do |uri|
  puts "Descargando: #{uri}"
  RuWEB::Read.init(uri)
end

# Descarga esta receta
raw = RuWEB::Read.init('https://pad.programando.li/breros:como-lo-hacemos/download')

# Contenido para archivar la receta
source = RuWEB::SewSource.init(raw)
code = RuWEB::SewCode.init(raw)

# Contenido para sitio
index = raw.gsub(/\u0023 Cómo.+/, '')
           .gsub(/(\u0060{3}).+/, "\u0060\u0060\u0060{.numberLines}")

# Contenido para ebooks
epub = pads[0..-2].join("\n")

# Contenido para notas
notes = pads.last

# Contenido para pdfs
pdf = epub.gsub(/<\/*section[^>]*?>/, '')

# Guardado de archivos
RuWEB::Save.init('README.md', source)
RuWEB::Save.init('ruweb.log', code)
RuWEB::Save.init('public/index.md', index)
RuWEB::Save.init('in/md/for_ebooks.md', epub)
RuWEB::Save.init('in/md/notas.md', notes)
RuWEB::Save.init('in/md/for_pdfs.md', pdf)
```

Manda a ejecutar:

```shell= _arregla_inputs
ruby << EOF
_script
EOF
```

### Toques finales

Producción de sitio:

```shell= _produce_sitio
pandoc -s -o public/index.html public/index.md
rm public/index.md
```

Modificación de archivos finales:

```shell= _mueve_outputs
mv ebooks/epub-*_* out/lo-que-hacemos.epub
mv ebooks/mobi-* out/lo-que-hacemos.mobi
mv in/md/for_pdfs.pdf out/lo-que-hacemos.pdf
mv in/md/for_pdfs-book.pdf out/lo-que-hacemos_folleto.pdf
```

Modificación de directorios:

```shell= _limpia_repo
rm -rf logs
mv ebooks/logs logs
mv logs/log-all.txt logs/pecas.log
mv ruweb.log logs
rm -rf ebooks
rm -rf pdfs
```

## Licencia

Este texto está bajo [Licencia Editorial Abierta y Libre (LEAL)](https://programando.li/bres). Con LEAL eres libre de usar, copiar, reeditar, modificar, distribuir o comercializar bajo las siguientes condiciones:

* Los productos derivados o modificados han de heredar algún tipo de LEAL.
* Los archivos editables y finales habrán de ser de acceso público.
* El contenido no puede implicar difamación, explotación o vigilancia.